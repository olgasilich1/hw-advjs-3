//Прототипное наследование помогает создать новый обьект
// на основании уже существующего обьекта

class Employee {
  constructor(name, age, salary) {
    this._name = name;
    this._age = age;
    this._salary = salary;
  }
  get name() {
    return this._name;
  }
  set name(value) {
    this._name = value;
  }
  get age() {
    return this._age;
  }
  set age(value) {
    if (Number.isInteger(value)) {
      this._age = value;
    } else {
      console.log('anabled value');
    }
  }
  get salary() {
    return this._salary;
  }
  set salary(value) {
    if (Number.isInteger(value)) {
      this._salary = value;
    } else {
      console.log('anabled value');
    }
  }
}
let employee = new Employee('adfa', 33, 500000);
// employee.age = 22;
console.log(employee);

class Programmer extends Employee {
  constructor(name, age, salary, lang) {
    super(name, age, salary);
    this.lang = lang;
  }

  get salary() {
    return this._salary;
  }
  set salary(value) {
    if (Number.isInteger(value)) {
      this._salary = value * 3;
    } else {
      console.log('anabled value');
    }
  }
}

let programmer1 = new Programmer('Jkfd', 22, 100000, 'UK');
programmer1.salary = 10;
console.log(programmer1.salary);
